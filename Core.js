const config = require('./config');
const Express = require('express')();
const db = require('./db');

Express.use(require('body-parser').urlencoded({ extended: false }));
Express.use(require('body-parser').json({ type: 'application/json' }));

Express.post('/upload', async (request, response) => {
    const { text } = request.body;
    try {
        const words = text.match(/(\S*)/gmi).filter(item => item !== '' && item).map(item => ({ text: item.toLowerCase() }));
        let queries = [];
        let newwords = 0;
        let newcombos = 0;
        let newwombos = 0;
        words.unshift({ text: '//START//' });
        words.push({ text: '//END//' });
        words.forEach((word, index) => queries.push(new Promise(async (resolve, reject) => {
            await db.makeQuery('INSERT IGNORE INTO words(value) VALUES(?)', [word.text]);
            const dbdata = (await db.makeQuery('SELECT * FROM words WHERE value=?', [word.text]) || [])[0];
            newwords++;
            word.id = dbdata.id;
            resolve();
        })));
        await Promise.all(queries);
        const combos = [];
        queries = [];
        words.forEach((word, index) => queries.push(new Promise(async (resolve, reject) => {
            if (index < words.length-1) {
                await db.makeQuery('INSERT IGNORE INTO chains(F, T, type) VALUES(?, ?, ?)', [word.id, words[index+1].id, 'T0']);
                const combo = (await db.makeQuery('SELECT id FROM chains WHERE F=? AND T=? AND type=?', [word.id, words[index+1].id, 'T0']))[0];
                newcombos++;
                combos[index] = combo;
                resolve();
            } else resolve()
        })));
        await Promise.all(queries);
        const wombos = [];
        queries = [];
        combos.forEach((combo, index) => queries.push(new Promise(async (resolve, reject) => {
            if (index < combos.length-1) {
                await db.makeQuery('INSERT IGNORE INTO chains(F, T, type) VALUES(?, ?, ?)', [combo.id, combos[index+1].id, 'T1']);
                const wombo = (await db.makeQuery('SELECT id FROM chains WHERE F=? AND T=? AND type=?', [combo.id, combos[index+1].id, 'T1']))[0];
                newwombos++;
                wombos[index] = wombo;
                resolve();
            } else resolve();
        })));
        await Promise.all(queries);
        response.json({ success: true, newwords, newcombos, newwombos })
    } catch (error) {
        console.error(error);
        response.json({ success: false, error: error.message })
    }
})

Express.get('/get', async (request, response) => {
    response.send(await require('./generator')());
})


Express.get('/', async (request, response) =>  {
    response.sendFile(`${__dirname}/index.html`);
})

Express.listen(config.port);