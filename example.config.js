module.exports = {
    port: 3000,
    db: {
        host: "localhost",
        user: "root",
        password: "",
        database: "qwent",
        timezone: "Z",
        connectionLimit: 100,
    },
}