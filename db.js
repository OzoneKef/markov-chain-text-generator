const mysql = require('mysql');
const config = require('./config');

class DB {
    constructor() {
        this.pool = mysql.createPool(config.db)
    }

    makeQuery(query, args){
        

        return new Promise(async (resolve, reject) => {
            this.pool.getConnection((err, connection) => {
                if (err) reject(err);
                connection.query(query, args, (error, results, fields) => {
                    connection.release()
                    if (!error){
                        resolve(results)
                    } else {
                        reject(error)
                    }
                });
            })
        })
    }
}

const DBresolver = new DB

module.exports = DBresolver