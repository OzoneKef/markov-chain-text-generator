const db = require('./db');
Array.prototype.r = function() {return this[Math.floor(Math.random()*this.length)]};

module.exports = async () => {
    let string = '';

    const start = (await db.makeQuery('SELECT * FROM words WHERE value=?', ['//START//']))[0];
    const end = (await db.makeQuery('SELECT * FROM words WHERE value=?', ['//END//']))[0];

    let current = (await db.makeQuery('SELECT * FROM chains WHERE F=? AND type=?', [start.id, 'T0'])).r();
    string += (await db.makeQuery('SELECT value FROM words WHERE id=?', [current.F]))[0].value
    while (current.T !== end.id) {
        let wombo = (await db.makeQuery('SELECT * FROM chains WHERE F=? AND type=?', [current.id, 'T1'])).r();
        current = (await db.makeQuery('SELECT * FROM chains WHERE id=?', [wombo.T]))[0];
        string += ' '+(await db.makeQuery('SELECT value FROM words WHERE id=?', [current.F]))[0].value
    }
    string += ' '+(await db.makeQuery('SELECT value FROM words WHERE id=?', [current.T]))[0].value

    return string;
}